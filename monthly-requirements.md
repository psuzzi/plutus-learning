## Each month, the Plutus Project Based Learning (Plutus PBL) team will collaborate to:
- complete the monthly list of PBL tasks
- write the next month's list of tasks
    - For first month, we will test using GitLab Issue Tracking
- provide documentation in the form of:
    1. written documentation
    2. video walkthroughs (James will coordinate)
    3. interactive front ends where anyone can explore proofs of concept
- Team retros: let's run weekly for the first month, then change if necessary for second month

## How will we handle fundamentals?
- How can we create documentation for newcomers so that they can understand eUTXO model, for example?
    - How can I visualize the ledger?
    - How does interaction with Cardano work (ie what endpoints are available?)
    - In general: provide the holistic picture / architecture...

## James will facilitate
- weekly office hours
- monthly updates
- creation of video documentation

## Partnerships
Longer term, we are building a model for additional start-ups to sponsor additional Plutus PBL devs. This group serves as an initial example for a model that, we expect, will scale.
