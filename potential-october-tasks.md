# What should we cover in October?

## Focus on what we can do without PAB
- Where can we store scripts?
- What is the minimum necessary stack for interaction with Cardano Mainnet?


## Needed for Bonfire (1:1) Dapp
Can we build each of the following without using the PAB?

### Simple event escrow contract:
- at moment of booking, Attendee sends funds to escrow contract
- when event happens, the Organizer says that it happened, and funds are given to Organizer

### Simple event escrow contract with time constraints: 
- we'll handle time so that Organizer cannot claim funds until after end time of event

### Later: dispute resolution
- Not in scope in this month, but we must be thinking about it


## Working Token Sale that is Mainnet Ready

## Any sorts of auctions we can cover?