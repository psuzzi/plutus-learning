# Compiling Plutus Scripts
### (and related notes)

- drafted by James
- 2021-09-22

### Necessary files
In order to successfully compile a Plutus script, we need the following files:
```
/project-folder/plutus-<contractname>.cabal
/project-folder/src/<ContractName>.hs
/project-folder/app/plutus-<contractname>.hs
```

The file names above are based on conventions we can see in https://github.com/input-output-hk/Alonzo-testnet/, where:

- `<ContractName>.hs` is the Plutus code to be compiled
- `plutus-<contractname>.hs` is a boilerplate file that runs the compilation

We must change some details in `plutus-<contractname>.cabal` and `plutus-<contractname>.hs`.

### In `plutus-<contractname>.cabal`, look for:
- Metadata: `name`, `desc`, `author`, etc
- `exposed modules` will match the module name in `<ContractName>.hs`
- `executable`
    - `main-is` should be `plutus-<contractname>.hs`
    - `build-depends`, use `plutus-<contractname>` to match corresponding file
    - And of course make sure the `hs-source-dirs` matches our directory structure

### In `plutus-<contractname>.hs`, look for:
- `import` the module name in `<ContractName>.hs`, and include the right script functions
- In `main`, we'll change the names of of some parameters to match the imported functions

### With these changes made, we should be able to:
```
cd project-folder
cabal build -w ghc-8.10.4
cat CABAL_FILE | grep executable

cabal run NAME_OF_EXECUTABLE -- 1 CONTRACT_NAME.plutus
```