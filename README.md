# Plutus Project-Based Learning at Gimbalabs

An open repository where we can collaboratively learn Plutus and build examples to share across the Cardano community.

## Gimbalabs Intentions
1. Create a space where anyone can develop a deeper understanding and get better at building with Plutus.
2. Build an example of a repository of reusable, open source Plutus components (Apache 2.0 License) that will benefit the Cardano community.
3. Practice working together as Gimbalabs begins to take on commercial opportunities.

## Plutus Learning Tasks

This list of tasks assumes that you've completed the most recent iteration of [Plutus Pioneers](https://github.com/input-output-hk/plutus-pioneer-program), and you're ready to start integrating Cardano's smart-contract functionality into a full development stack.

# What did we build in September, 2021?

## 00: Start Here - get Plutus Starter up and running
#### Intial Task Description:
- A basic example of how we'll interact with the Plutus Application Backend (PAB) in a web-based dapp
- Official Repo: https://github.com/input-output-hk/plutus-starter
- You can use the Plutus Starter project as a starting point for what you'll see in the following tasks.

## 01a: Interact with a basic contract through an endpoint.
#### Intial Task Description:
- What is the minimal contract we can use to demonstrate interaction with a contract via a local endpoint?
- Task: implement the "Gift" contract from Lecture 2 of Plutus Pioneers, in a way that we can interact with it via the PAB
#### Video Documentation:
https://youtu.be/UbTOZWr1-yM

## 01b: Parameterized "Grab"
#### Intial Task Description:
- Task: create a parameterized version of the Grab endpoint and practice accessing it via `curl`
#### Video Documentation:
https://www.youtube.com/watch?v=yeZE5MAjFTI
#### Public Repository:
https://github.com/eselkin/param-pb-pab

## 02: Deliver an Authentication NFT to a Wallet
#### Intial Task Description:
- Review Lecture 5 and build the simplest possible interaction point for getting something like an auth-nft into a simulated wallet.
- We can implement what we saw in Lecture 5. The goal is for an end user to obtain an authentication token by interacting with a web front-end.
#### Video Documentation:
https://youtu.be/NBf8nezLIaU
#### Public Repositories:
- https://github.com/SamJeffrey8/simple-nft-minter
- https://github.com/SamJeffrey8/simple-nft-minter-site


## 03: Use an Authentication NFT to access Encrypted Information
#### Intial Task Description:
- Use an NFT as an authentication token for some privileged access
#### Video Documentation:
https://youtu.be/v7QZsDbpy5M
#### Public Repository:
https://github.com/ganeshnithyanandam/OAuth-NFT

# Our Current Work: October, 2021

## This month, our outcomes will include the following
- Continue to create video documentation for each task
- Continue to publish public repositories sharing our outcomes
- To the extent possible, we'll build live web demos. When this is not possible, we'll talk about what we need in order to get to that point, which will inform future tasks. 
- Worth covering:
  - PAB vs. referencing compiled scripts
  - Testnet CSK with front-end
- Maybe a little something about favorite misconceptions or common pitfalls? Our goal is practice talking about these new tools so that we can achieve a level of semantic clarity.

# PPBL Tasks for October 2021:

## 01a: Check that an NFT exists in a Wallet 
- Carefully defining "Wallet" as a collection of UTXOs (Address / Wallet / transaction / UTXO)
## 01b: Get a list of NFTs in a given UTXO / Wallet

## 02: Implement OAuth with NFTs
- Building upon tasks 02 and 03 in September, how can we move a few steps closer to an implementation that people actually use?
- Example: an organization/brand can distribute NFTs that represent the issuer; build the mechanism for checking whether that NFT exists.

## 03: Applied Escrow contract with all necessary endpoints for interaction
- Alice is (for example) a Plutus expert
- Bob needs help with Plutus
- Alice is offering 60 minute blocks of time where she offers 1:1 support on Plutus, at a rate of xx Ada/hour
- Bob books an hour with Alice. When booking time, he places xx Ada in this escrow contract
  - After booking the hour, Bob can see specific availability for Alice and can:
  - book time, or
  - get refunded (if no times are compatible)
- Alice meets with Bob
- ...someone says "that meeting happened"
- Funds are released from escrow to Alice

## 04 (if time allows): Add dispute resolution on #03
- drafting standards so that we can begin to refine
- mediation vs arbitration
- fact finding / data / 
- "meeting didn't happen" vs. "this meeting happened, but there wasn't enough value, I didn't get my money worth"
- thinking ahead: scaling toward higher-value transactions/interactions; standards for different cases/domains

## Continue to check out Alonzo Purple Exercises
- Can we contribute our own take on some of the solutions to Exercise #5 and beyond?
- Github repo: https://github.com/input-output-hk/Alonzo-testnet


## Archive

### At least for now, we are setting these tasks aside:

#### 04: Update Plutus Pioneer Vesting exercise 
- How can we help developers to understand how time is handled on the Cardano Protocol?
- What tools can we build to make it easier to work with time constraints in Plutus scripts?
- Plutus is in continuous evolution, and as a result the cardano-api can, from time to time, break backward compatibility 
  because of new findings or updates. In this occasion, `SlotRange` in the `TxInfo` has been replaced by `POSIXTimeRange` 
  [commit](https://github.com/input-output-hk/plutus/commit/fbcb8b787a9bcc7ecccb914f9db05a4ac7efb779#diff-7fb2bda211144200604187079d6c51f5be6dcac75e8a2bc2d9b818758c349c3a).
  As an exercise, update the code of `Week3/Homework1` accordingly. Try to put then the Cardano Developer hat on and try to explain why this was necessary and what complications it raises.

#### 05: Metadata validation proof of concept
- How can we help developers understand the difference between "transaction metadata" and the "datum" in a Plutus script?
- Build a proof concept that illustrates the difference, highlighting the utility and the limitations of each.

### Optional Background: Learn a bit about Servant.API
- Tutorial: https://docs.servant.dev/en/stable/tutorial/index.html#cabal-install
- Docs: https://hackage.haskell.org/package/servant
