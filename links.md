# Links to solutions in progress:

- Gift from Matthias: https://gist.github.com/manonthemat/ffcf81a07c966cd1a1a061757c71540e
- Gift from Eli: https://gist.github.com/eselkin/ab648a553bb784f5a3bfedcd2f49dcf8
- Shared Piggy Bank from Matthias: https://github.com/manonthemat/shared-piggy-bank
- Shared Piggy Bank 2 from Ganesh: https://github.com/ganeshnithyanandam/shared-piggy-bank-2
- NFT provider from Sam: https://gist.github.com/SamJeffrey8/7417f41f0a3004d97c889618ca5be042
