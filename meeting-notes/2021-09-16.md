# Notes

## PAB Proxy Server
Sam is looking at options for setting up a proxy on the client side. Investigating CORS.

### Links:
- https://medium.com/bb-tutorials-and-thoughts/react-how-to-proxy-to-backend-server-5588a9e0347
- https://create-react-app.dev/docs/proxying-api-requests-in-development/

Ganesh: When we say proxy - our goal is to access the APIs that are exposed by PAB. What are the the necessary layers in-between? 

## Regarding video documentation
- Ganesh: we need an introduction; use the guidebook that Ganesh is preparing.
- How about the lobster challenge? --> Friday at 
- Angela: as of Tuesday only ~100 people had completed the Lobster challenge
- Some of what we do with PAB will require people to run their own PAB (ALL TEAM)
- Other video content will be public, James will own this, but all are welcome

## Dev-Facing Video Outline:
- Here's a POC of what you can do
- Here's what the PAB is showing us - live look at PAB server running - contract instance IDs
- Here's the code and where you can find it
- Here's what we recommend as next steps!

## Public-facing (CSK style) videos
- Let's open our minds to what these tools can do. We'll test some ideas by playing with the [lobster challenge](https://github.com/input-output-hk/lobster-challenge) on Friday. We'll begin to generalize over the next few weeks.

## Parameterised Piggybank
- Troubleshooting example scripts, we found that when a wallet has insufficient amount, it seems that we get an error state, because it is rejected by the validator. Of course we want a wallet to be able to make more than one attempt to empty the piggy bank. Put another way, we want to avoid a case where if we want to try to withdraw from the piggybank before it has sufficient funds, then we're "locked out" indefinitely.
- It seems we have to start the contract again. The error terminates the contract, so we must start it again. To do so, we simply called `empty` again at the end of the end of itself. This compiled, but it didn't change the behavior of the contract.
- We're still troubleshooting this part. A wallet should be able to attempt to empty the piggybank, then still be "valid" even after an unsuccessful attempt. More generally, we want a wallet to still be viable after any error. So we can destroy the instance, but then allow the same wallet to start a new instance.
- What if throw an error, but skip the step of submitting any tx? Real world version: I don't care how many times my kids shake the piggybank, all I really need to know is when money moves in and out. We can call this the "forgiving" version of the contract.
- OR - we have to activate a new instance each time an `empty` is attempted. We could call this "unforgiving" version of the contract.
- Query status first?
- The
- In a video, can we show what makes a wallet end up in an error state? And then how to address this error?

## Next Steps:
- Initial front end for any contract - Sam, do you have ideas + want to make an initial attempt? - Sam
- Lobster CSK - Friday: AWG, Matthias, James, and anyone else who can make it.
- Refine guidebook by middle of week - Ganesh
- Intro video: let's record next week. We can do this during our scheduled time on Wednesday, or sooner. - James will coordinate offline?
- Ask about error state issue on StackExchange - Eli & Ganesh
