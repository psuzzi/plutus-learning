## Create an overview guidebook for the journey into being a Plutus developer
- Ganesh started this work here: https://docs.google.com/document/d/1hOQl7Ne1JjFbs3sVGaVmmuU8W7gkHTvkayMJUSH3i9I/edit

## For each task:
1. Create a final draft of well-commented code
2. Write a `README.md` with usage, tips, and links to additional docs/resources
3. Record a quick (5-10 min) video showing what the code does and how to use it
4. Add links to any hosted front-ends that people can tinker with (when available, we'll look at these interfaces in any videos we create)
